function upload() {
	var files = document.getElementById("file").files;
	if (files.length == 0) return;
	var i = 0;
	(function nextFile() {
		var file = files[i++];
		if (!file) {
			location.reload();
			return;
		};
		var ws = new WebSocket(`${location.href.replace('http','ws')}?name=${encodeURIComponent(file.name)}`);
		var reader = file.stream().getReader();
		var bytesSent = 0;
		function nextChunk(){
			reader.read().then(function({value}) {
				if (!value) {
					ws.send("end");
					ws.close();
					nextFile();
					return;
				}
				ws.send(value);
				bytesSent += value.length;
				setStatus(`uploading ${file.name} (${((bytesSent/file.size)*100).toFixed(2)}%)`);
			});
		}
		ws.onopen = () => nextChunk();
		ws.onmessage = event => {
			if (typeof event.data == "string") setStatus(`server: ${event.data}`);
			else nextChunk();
		};
		ws.onerror = error => setStatus(error.message);
	})();
}
function setStatus(x) {
	document.getElementById('form').innerText = x;
}